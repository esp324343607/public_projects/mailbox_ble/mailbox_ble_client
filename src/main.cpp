#include <Arduino.h>
#include "BLEDevice.h"
#include <LiquidCrystal_I2C.h>  // https://registry.platformio.org/libraries/marcoschwartz/LiquidCrystal_I2C
#include "NO_BLOCK_BLINK.h"     // https://registry.platformio.org/libraries/dmack/NO_BLOCK_BLINK

static LiquidCrystal_I2C lcd(0x27, 16, 2);
static BLEUUID serviceUUID("54d7df80-11fe-11ee-be56-0242ac120002");
static BLEAdvertisedDevice* advertisedDevice;
static boolean blinkLED = false;
static boolean debug = false;
static boolean isConnected = false;
static boolean resetButtonPressed = false;
static boolean shouldConnect = false;
static boolean shouldScan = false;
static const uint16_t LED_PIN = 2;
static const uint16_t RESET_BUTTON_PIN = 5;
static const uint8_t PIEZO_PIN = 4;
static const uint8_t PIEZO_PWM_CHANNEL = 3;
static const unsigned long BUTTON_DEBOUNCE_TIME = 1000000;
static portMUX_TYPE resetButtonMux = portMUX_INITIALIZER_UNLOCKED;
static volatile unsigned long resetButtonLastMicros;
NO_BLOCK_BLINK blinker = NO_BLOCK_BLINK();

int tempo = 200;
int melody[] = {
  // Nokia Ringtone
  // Score available at https://musescore.com/user/29944637/scores/5266155
  NOTE_E, 8, 5, NOTE_D, 8, 5, NOTE_Fs, 4, 4, NOTE_Gs, 4, 4,
  NOTE_Cs, 8, 5, NOTE_B, 8, 4, NOTE_D, 4, 4, NOTE_E, 4, 4,
  NOTE_B, 8, 4, NOTE_A, 8, 4, NOTE_Cs, 4, 4, NOTE_E, 4, 4,
  NOTE_A, 2, 4,
};
int notes = sizeof(melody) / sizeof(melody[0]) / 2;
int wholenote = (60000 * 4) / tempo;
int divider = 0, noteDuration = 0, octave = 0;

//===============================================================================
// Interrupts
//===============================================================================
void IRAM_ATTR onResetButtonPress() {
    portENTER_CRITICAL_ISR(&resetButtonMux);
    uint32_t currentTime = micros();
    bool buttonIsDebounced = currentTime - resetButtonLastMicros >= BUTTON_DEBOUNCE_TIME;
    if (buttonIsDebounced) {
        resetButtonPressed = true;
    }
    resetButtonLastMicros = currentTime;
    portEXIT_CRITICAL_ISR(&resetButtonMux);
}

//===============================================================================
// Indicators
//===============================================================================
void playTone() {
    // source: https://github.com/robsoncouto/arduino-songs/blob/master/nokia/nokia.ino
    for (int thisNote = 0; thisNote < notes * 3; thisNote = thisNote + 3) {
        divider = melody[thisNote + 1];
        octave = melody[thisNote + 2];
        
        if (divider > 0) {
            noteDuration = (wholenote) / divider;
        } else if (divider < 0) {
            noteDuration = (wholenote) / abs(divider);
            noteDuration *= 1.5;
        }
        ledcWriteNote(PIEZO_PWM_CHANNEL, note_t(melody[thisNote]), octave+1);
        delay(noteDuration);
        ledcWrite(PIEZO_PWM_CHANNEL, 0);
    }
}

void setupLCD() {
    lcd.init();
    lcd.backlight();
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Home (client)");
}

void indicateMailDelivered() {
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("You have new");
    lcd.setCursor(0,1);
    lcd.print("mail waiting.");
    if (debug) Serial.println(F("(E) Mailbox opened (disconnecting)."));
    blinkLED = true;
    playTone();
}

void indicateResetButtonPressed() {
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Mailbox");
    lcd.setCursor(0,1);
    lcd.print("reset.");
    if (debug) Serial.println(F("\n[X] Reset button pressed.\n"));
    blinkLED = false;
    digitalWrite(LED_PIN, LOW);

}

//===============================================================================
// Callbacks
//===============================================================================
class AdvertisedDeviceCallbacks : public BLEAdvertisedDeviceCallbacks {
    /**
     * This code defines a function onResult that takes in a BLEAdvertisedDevice
     * object as input. It checks if the device has a specific service UUID and
     * is advertising that service. If it is, the code stops scanning for BLE
     * devices, saves the advertised device, and sets flags to connect and scan.
     */
    void onResult(BLEAdvertisedDevice device) {
        if (device.haveServiceUUID() && device.isAdvertisingService(serviceUUID)) {
            if (debug) Serial.println(F("(C) Advertised device found."));
            BLEDevice::getScan()->stop();
            advertisedDevice = new BLEAdvertisedDevice(device);
            shouldConnect = true;
            shouldScan = true;
        }
    }
};

class ClientCallbacks : public BLEClientCallbacks {
    void onConnect(BLEClient* client) {
        isConnected = true;
        if (debug) Serial.println(F("(D) Connected to server."));
    }

    void onDisconnect(BLEClient* client) {
        indicateMailDelivered();
        isConnected = false;
        attachInterrupt(digitalPinToInterrupt(RESET_BUTTON_PIN), onResetButtonPress, FALLING);
    }
};

//===============================================================================
// Locate and connect to a BLE server
//===============================================================================
void scanForServer() {
    if (debug) Serial.println(F("(B) Scanning ..."));
    BLEScan* scanner = BLEDevice::getScan();
    scanner->setAdvertisedDeviceCallbacks(new AdvertisedDeviceCallbacks());
    scanner->setInterval(1349);
    scanner->setWindow(449);
    scanner->setActiveScan(true);
    scanner->start(0); // Blocking method
}
/**
 * Connects to a BLE server with the specified service UUID.
 * Returns true if the connection was successful, false otherwise
 */
bool connectToServer() {
    BLEClient* client  = BLEDevice::createClient();
    client->setClientCallbacks(new ClientCallbacks());
    client->connect(advertisedDevice);
    client->setMTU(517);

    BLERemoteService* service = client->getService(serviceUUID);
    if (service == nullptr) {
        if (debug) Serial.print(F("Failed to find specified service UUID: "));
        if (debug) Serial.println(serviceUUID.toString().c_str());
        client->disconnect();
        return false;
    }
    return true;
}

//===============================================================================
// Main
//===============================================================================
void setup() {
    Serial.begin(115200);
    BLEDevice::init("BLE_CLIENT_HOME");
    if (debug) Serial.println(F("\n(A) BLE client application started."));
    pinMode(LED_PIN, OUTPUT);
    pinMode(RESET_BUTTON_PIN, INPUT_PULLUP);
    ledcAttachPin(PIEZO_PIN, PIEZO_PWM_CHANNEL);
    digitalWrite(LED_PIN, LOW);
    setupLCD();
    scanForServer(); // Blocking. Must come last in setup
}

void loop() {
    if (shouldConnect) {
        shouldConnect = false;
        connectToServer();
    }

    if (resetButtonPressed && !isConnected && shouldScan) {
        resetButtonPressed = false;
        detachInterrupt(digitalPinToInterrupt(RESET_BUTTON_PIN));
        indicateResetButtonPressed();
        shouldScan = false;
        scanForServer();
    }

    if (blinkLED) {
        blinker.blink(LED_PIN, 4, 250, 750);
    }
}
