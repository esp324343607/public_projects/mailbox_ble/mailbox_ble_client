# Mailbox BLE Client

![Mailbox BLE Client schematic](images/mailbox-ble-client-schematic.png)

Gerber files for PCB fabrication are available for inspection, download, and ordering at [OSHPark](https://oshpark.com/shared_projects/IPdSX086).